public class DoubleMergeSort {
	public static int[] sort(double[] values){
		int[] order = new int[values.length];
		for (int i = 0; i < order.length; i++){
			order[i] = i;
		}
		mergeSort(values, order, 0, order.length, false);
		return order;
	}

	public static int[] reverseSort(double[] values){
		int[] order = new int[values.length];
		for (int i = 0; i < order.length; i++){
			order[i] = i;
		}
		mergeSort(values, order, 0, order.length, true);
		return order;
	}

	private static void mergeSort(double[] values, int[] order, int lo, int hi, boolean reverse){
		if (hi - lo < 2){
			return;
		}
		else {
			int mid = (hi + lo) / 2;
			mergeSort(values, order, lo, mid, reverse);
			mergeSort(values, order, mid, hi, reverse);
			merge(values, order, lo, hi, reverse);
		}
	}

	private static void merge(double[] values, int[] order, int lo, int hi, boolean reverse) {
		int mid = (hi + lo) / 2;
		int x = lo;
		int y = mid;
		int[] arr = new int[hi-lo];
		int i = 0;
		while (x < mid && y < hi){
			if (reverse == false){
				if (values[order[x]] < values[order[y]])
					arr[i++] = order[x++];
				else
					arr[i++] = order[y++];
			}
			else {
				if (values[order[x]] > values[order[y]])
					arr[i++] = order[x++];
				else
					arr[i++] = order[y++];
			}
		}
		while (x < mid){
			arr[i++] = order[x++];
		}
		while (y < hi){
			arr[i++] = order[y++];
		}
		for (i = 0; i < arr.length; i++){
			order[lo+i] = arr[i];
		}

	}
}