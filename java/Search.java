import java.io.BufferedReader;
import java.io.FileReader;
import java.util.HashMap;
import java.util.HashSet;

public class Search {

    final static boolean quiet = true;
    int numDocs;
    String[] titleArray;
    Retriever articleRetriever;
    Retriever titleRetriever;
    double[] pageRanks;
    double[] searchScore;
    double[] titleSearchScore;
    
    public Search(String titles, String ranks, Retriever articleRetriever, Retriever titleRetriever, int numDocs) throws Exception {
    	this.articleRetriever = articleRetriever;
    	this.titleRetriever = titleRetriever;
        this.numDocs = numDocs;
        titleArray = new String[numDocs];
        pageRanks = new double[numDocs];
        
        /*
         * Load our array of titles and our array of pageRanks
         */
        BufferedReader titleReader = new BufferedReader(new FileReader(titles));
        BufferedReader rankReader = new BufferedReader(new FileReader(ranks));
        for(int i = 0; i < numDocs; i++){
            this.titleArray[i] = titleReader.readLine();
            pageRanks[i] = Double.parseDouble(rankReader.readLine());
        }
    }
   
    public void printRankedResults(String query) throws Exception {
        searchScore = getRanks(query, articleRetriever);
        titleSearchScore = getRanks(query, titleRetriever);
        double[] ranks = new double[numDocs];
        double alpha = 1;
        double beta = 1;
        double gamma = .7;

        for(int i = 0; i < numDocs; ++i)
        {
            ranks[i] =
               searchScore[i] + titleSearchScore[i] + Math.log10(pageRanks[i]);
        }

        HashSet<String> unwantedPrefixes = new HashSet<String>();
        unwantedPrefixes.add("Wikipedia:");
        unwantedPrefixes.add("Category:");
        unwantedPrefixes.add("Portal:");
        unwantedPrefixes.add("Template:");
        unwantedPrefixes.add("Book:");
        unwantedPrefixes.add("File:");
        unwantedPrefixes.add("Help:");
        unwantedPrefixes.add("MediaWiki:");

        int[] order = DoubleMergeSort.reverseSort(ranks);
        if(!quiet)
            System.out.println("\n\033[1mCombinedScore\t\tSearchScore\t\tTitleSearchScore\tPageRank\t\tArticleTitle\033[0m ");
        int successful = 0;
        HashSet<String> set = new HashSet<String>();
        for(int i = 0; i < ranks.length && successful < 20; ++i){
            String title = titleArray[order[i]];
            int prefixIndex = title.indexOf(':');
            if(!unwantedPrefixes.contains(title.substring(0, prefixIndex + 1))){
	            if (!set.contains(getCleanWords1(title)) && !set.contains(getCleanWords2(title))) {
                    if(!quiet)
                    {
            	        System.out.println(ranks[order[i]] + "\t" +
                            searchScore[order[i]] + "\t" + titleSearchScore[order[i]] +
                            "\t" + Math.log10(pageRanks[order[i]]) + "\t" + title);
                    }
                    else
                    {
                        System.out.println(title);
                    }
            	    ++successful;
            	    set.add(getCleanWords1(title));
            	    set.add(getCleanWords2(title));
        	    }
            }
        }
        if(!quiet)
            System.out.println();
    }

    public static String[] getArrayOfWords(String pageText) {
        return pageText.toLowerCase().replaceAll("[[\\W]&&[\\S]]", " ").split("\\s");
    }

    public static String getCleanWords1(String pageText) {
        return pageText.toLowerCase().replaceAll("[[\\W]&&[\\S]]", "");
    }

    public static String getCleanWords2(String pageText) {
        return pageText.toLowerCase().replaceAll("[[\\W]&&[\\S]]", " ");
    }

    public double[] getRanks(String query, Retriever r) throws Exception {
        HashMap<String, Integer> map = new HashMap<String, Integer>();
        double[] result = new double[numDocs];
        String[] words = getArrayOfWords(query);
        for(int i = 0; i < words.length; i++){
            if (!map.containsKey(words[i]))
                map.put(words[i], 0);
            map.put(words[i], map.get(words[i]) + 1);
        }
        for(String word : map.keySet()){
            if(word.length() > 0)
                getRanksForWord(result, word, map.get(word), r);
        }
        return result;
    }

    private void getRanksForWord(double[] result, String word, int qtf, Retriever r) throws Exception {
        int[][] docs = r.retrieve(word);
        if(docs == null)
        {
            System.out.println("KERNEL PANIC");
            System.exit(1);
        }
        if(!quiet)
            System.out.println("[ThunderSearch]: Getting ranks for \"" + word + "\" in " + r);
        int df = docs.length;
        for (int i = 0; i < df; i++)
            getRanksForWordAndDoc(result, word, qtf, docs[i], df, r.getLengths(), r.getAvdl());
    }

    private void getRanksForWordAndDoc(double[] ranks, String word, int qtf, int[] doc, int df, int[] lengths, double av) throws Exception {
        int docID = doc[0];
        double tf = doc[1];
        double dl = lengths[docID];
        
        double k1 = 1.5;
        double b = .75;
        double k3 = 500;
        
        double TF = ((k1 + 1.0) * tf) / ((k1 * ((1.0-b)+b*dl/av)) + tf);
        double IDF = (((double)numDocs) - df + 0.5) / (df + .05);
        double QTF = ((k3 + 1.0) * qtf) / (k3 + qtf);

        ranks[docID] += Math.log(IDF) * TF * QTF;
    }

    public static void main(String[] args) throws Exception{
    	int numDocs = 11930681;
    	Retriever r1 = new Retriever("../../cs410-final-data/ArticleIndex", numDocs);
    	Retriever r2 = new Retriever("../../cs410-final-data/TitleIndex", numDocs);
        Search search = new Search("../../cs410-final-data/ListOfTitles.txt", 
            "../../cs410-final-data/PageRanks.txt", r1, r2, numDocs);
        String query = "";
        for(String s: args)
            query += s + " ";
        query = query.substring(0, query.length() - 1);
        if(!quiet)
            System.out.println("[ThunderSearch]: running query \"" + query + "\"");
        search.printRankedResults(query);
    }
}
