import os
import sys

judgments = {}
allPages = open("all.txt", "r")
for line in allPages:
    line = line.strip()
    if not "#" in line and not ":" in line:
        print(line)
        while(True):
            try:
                score = input("relevance: ")
                # only accept 0 or 1!!
                if int(score) is 0 or int(score) is 1:
                    break
            except ValueError:
                continue
            except SyntaxError:
                continue
            except NameError:
                continue
        judgments[line] = score
allPages.close()

class SearchEngine:
    def __init__(self, filename, name):
        self.results = open(filename, "r")
        self.name = name
    def __del__(self):
        self.results.close()
    def getPrecision(self, judgments):
        score = 0
        length = 0
        for line in self.results:
            line = line.strip()
            if not "#" in line and not ":" in line:
                score = score + int(judgments[line])
                length = length + 1
        print(self.name + " Average Precision: " + str(score / length))

print("=============================================")
google = SearchEngine("google.txt", "Google")
google.getPrecision(judgments)
bing = SearchEngine("bing.txt", "Bing")
bing.getPrecision(judgments)
wikisearch = SearchEngine("wikisearch+ts.txt", "WikiSearch (Title Rank)")
wikisearch.getPrecision(judgments)
wikisearchNoTitle = SearchEngine("wikisearch-ts.txt", "WikiSearch (No Title Rank)")
wikisearchNoTitle.getPrecision(judgments)
wikipedia = SearchEngine("wikipedia.txt", "Wikipedia")
wikipedia.getPrecision(judgments)
thundersearch = SearchEngine("thundersearch.txt", "ThunderSearch")
thundersearch.getPrecision(judgments)
print("=============================================")
