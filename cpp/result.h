#ifndef _RESULT_H_
#define _RESULT_H_

#include <string>

using std::string;

class Result
{
    public:

        Result(string title, size_t number, double rank, double score, double titleS, double cScore):
            pageTitle(title), pageNumber(number), pageRank(rank), lemurScore(score), titleScore(titleS), combinedScore(cScore) { /* nothing */ }

        static bool sortByCombined(const Result & first, const Result & second)
        {
            return first.combinedScore > second.combinedScore;
        }

        static bool sortByLemur(const Result & first, const Result & second)
        {
            return first.lemurScore > second.lemurScore;
        }

        static bool sortByPageRank(const Result & first, const Result & second)
        {
            return first.pageRank > second.pageRank;
        }

        static bool sortByTitleRank(const Result & first, const Result & second)
        {
            return first.titleScore > second.titleScore;
        }

        string pageTitle;
        size_t pageNumber; 
        double pageRank;
        double lemurScore;
        double combinedScore;
        double titleScore;
};

#endif
