#include <fstream>
#include <utility>
#include <iostream>
#include <string>
#include <sstream>
#include <unordered_map>
#include "client/dbclient.h"

using std::pair;
using std::ifstream;
using std::make_pair;
using std::string;
using std::unordered_map;
using std::cout;
using std::cerr;
using std::endl;
using namespace mongo;

/**
 * Reads in page ranks from a file.
 * The number on line i of the file is the page rank of the ith article
 * @param pageRankFile - the file containing page rank data
 * @return a mapping of article numbers to their page rank
 */
unordered_map<unsigned int, double> getRanks(string pageRankFile)
{
    unordered_map<unsigned int, double> ranks;
    ifstream rankFile(pageRankFile, ifstream::in);
    string line;
    unsigned int count = 0, total = 11930681;
    if(rankFile.is_open())
    {
        while(rankFile.good() && count <= total)
        {
            std::getline(rankFile, line);
            double rank;
            std::istringstream(line) >> rank;
            ranks.insert(make_pair(count++, rank));
            if(count % 10000 == 0)
            {
                cout << "Reading pagerank file: "
                     << (int)((double) count / (double) total * 100)
                     << "% \r" << std::flush;
            }
        }        
        cout << endl;
        rankFile.close();
    }
    else
    {
        cerr << "Failed to open " << rankFile << endl;
    }
    return ranks;
}

/**
 * Inserts the pagerank data into the mongo database.
 * @param ranks - a map of ranks (page id to rank) to insert
 */
void insertRanks(unordered_map<unsigned int, double> ranks)
{
    try
    {
        DBClientConnection mongo;
        mongo.connect("localhost");
        cout << "[Mongo] connected ok!" << endl;
        unsigned int count = 0, total = 11930681;
        unordered_map<unsigned int, double>::iterator it;
        for(it = ranks.begin(); it != ranks.end(); ++it)
        {
            BSONObj rank = BSON("_id" << it->first << "pageRank" << it->second);
            mongo.insert("pageRanks.ranks", rank);
            if(count++ % 10000 == 0)
            {
                cout << "Inserting into database: "
                     << (int)((double) count / (double) total * 100)
                     << "% \r" << std::flush;
            }
        }
        cout << endl;
    }
    catch(DBException & e)
    {
        cout << "[Mongo] " << e.what() << endl;
    }
}

int main(int argc, char* argv[])
{
    //string pageRankFile = "../cs410-final-data/PageRanks.txt";
    string pageRankFile = "/home/sean/java/WikiRank-data/PageRanks.txt";
    unordered_map<unsigned int, double> ranks = getRanks(pageRankFile);
    insertRanks(ranks);
}
