#include <iostream>
#include "wikiranker.h"

using std::cout;
using std::endl;

int main(int argc, char* argv[])
{
    if(argc < 2)
    {
        cout << "Type query after program name" << endl;
        return 1;
    }   

    vector<string> args(argv + 1, argv + argc);
    string query = "";
    for(size_t i = 0; i < args.size(); ++i)
        query += args[i] + " ";
    query.erase(query.begin() + query.size() - 1);

    WikiRanker ranker;
    ranker.runQuery(query);
    ranker.displayResults(20);
    
    return 0;
}
