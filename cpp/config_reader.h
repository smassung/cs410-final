#ifndef _CONFIG_READER_H_
#define _CONFIG_READER_H_

#include <unordered_map>
#include <iostream>
#include <fstream>
#include <string>

using std::ifstream;
using std::make_pair;
using std::string;
using std::unordered_map;
using std::cout;
using std::cerr;
using std::endl;

namespace ConfigReader
{
    unordered_map<string, string> read();
}

#endif
