#ifndef _WIKI_RANKER_H_

#include <algorithm>
#include <iostream>
#include <string>
#include <sstream>
#include <utility>
#include <vector>
#include <fstream>
#include <iomanip>
#include <unordered_map>
#include <unordered_set>
#include <float.h>

#include "result.h"
#include "config_reader.h"

// lemur
#include "indri/IndexEnvironment.hpp"
#include "indri/QueryEnvironment.hpp"
#include "lemur/Exception.hpp"

// mongo
#include "client/dbclient.h"

using std::ifstream;
using std::pair;
using std::vector;
using std::string;
using std::cout;
using std::cerr;
using std::endl;
using std::make_pair;
using std::unordered_map;
using std::unordered_set;
using namespace indri::api;
using namespace lemur::api;
using namespace mongo;
using namespace bson;

class WikiRanker
{
    public:

        /**
         * Constructor; initializes the lemur and mongodb environments.
         */
        WikiRanker();

        /**
         * Destructor; closes the lemur environment.
         */
        ~WikiRanker();

        /**
         * Runs a query on the lemur index and saves the results.
         * @param query - the query to run
         */
        void runQuery(string query);

        /**
         * Prints the ranked list of results to stdout.
         * @param numResults - the max number of results to display to the
         *  screen
         */
        void displayResults(size_t numResults);

    private:

        QueryEnvironment* env;
        QueryEnvironment* titleEnv;
        DBClientConnection mongo;
        vector<Result> results;
        unordered_set<string> unwantedPrefixes;

        size_t numOriginalResults;
        double lambda;
        string lemurIndex;
        string lemurTitleIndex;
        string useTitleScore;
        bool quiet;

        /**
         * Reads a config file and sets variables accordingly.
         */
        void readConfig();

        /**
         * Helper function for normalize; finds the max and min score
         *  for page ranks and lemur scores.
         */
        void findMaxMin(double & maxPR, double & minPR, double & maxLM, double & minLM, double & maxTS, double & minTS) const;

        /**
         * Determines whether a page title should be filtered.
         * @param title - the page title in question
         * @return whether it should be ignored in the results list
         */
        bool shouldFilter(string title) const;

        /**
         * Normalizes results in the range [0, 1].
         */
        void normalizeResults();

        /**
         * Takes the page rank into account for each result and resorts
         *  the results.
         */
        void adjustPageRank();
};

#endif
