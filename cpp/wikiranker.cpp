#include "wikiranker.h"

/**
 * Constructor; initializes the lemur and mongodb environments.
 */
WikiRanker::WikiRanker()
{
    readConfig();

    // set up lemur environment
    env = new QueryEnvironment();
    env->addIndex(lemurIndex);

    // set up lemur title environment
    titleEnv = new QueryEnvironment();
    titleEnv->addIndex(lemurTitleIndex);

    // set up mongodb environment
    try
    {
        mongo.connect("localhost");
        if(!quiet)
            cout << "[MongoDB]:    connected successfully" << endl;
    }
    catch(DBException & e)
    {
        cout << "[MongoDB]:    " << e.what() << endl;
    }

    // set up unwanted prefixes
    unwantedPrefixes.insert("Wikipedia");
    unwantedPrefixes.insert("Category");
    unwantedPrefixes.insert("Portal");
    unwantedPrefixes.insert("Template");
    unwantedPrefixes.insert("Book");
    unwantedPrefixes.insert("File");
    unwantedPrefixes.insert("Help");
    unwantedPrefixes.insert("MediaWiki");
}

/**
 * Destructor; closes the lemur environment.
 */
WikiRanker::~WikiRanker()
{
    env->close();
    delete env;
    titleEnv->close();
    delete titleEnv;
}

/**
 * Runs a query on the lemur index and saves the results.
 * @param query - the query to run
 */
void WikiRanker::runQuery(string query)
{
    if(!quiet)
        cout << "[WikiRanker]: running query \"" << query << "\"" << endl;

    vector<ScoredExtentResult> lemurResults = env->runQuery(query, numOriginalResults);
    vector<string> names = env->documentMetadata(lemurResults, "docno");

    vector<ScoredExtentResult> lemurTitleResults = titleEnv->runQuery(query, numOriginalResults);
    vector<string> titleNames = titleEnv->documentMetadata(lemurTitleResults, "docno");

    // put title scores into hashtable
    unordered_map<string, double> titleToScore;
    for(size_t i = 0; i < titleNames.size(); ++i)
    {
        size_t breakIndex = names[i].find(" ");
        string articleNumberStr = names[i].substr(0, breakIndex);
        string articleTitle = names[i].substr(breakIndex + 1, names[i].size());

        titleToScore.insert(make_pair(articleNumberStr, lemurTitleResults[i].score));
    }

    for(size_t i = 0; i < names.size(); ++i)
    {
        size_t breakIndex = names[i].find(" ");
        string articleNumberStr = names[i].substr(0, breakIndex);
        string articleTitle = names[i].substr(breakIndex + 1, names[i].size());

        unsigned int articleNumber;
        std::istringstream(articleNumberStr) >> articleNumber;

        bo mongoResult = mongo.findOne("pageRanks.ranks", QUERY("_id" << articleNumber));
        double pageRank = mongoResult["pageRank"].Double();
        double combinedScore = 0.0;

        Result result(articleTitle, articleNumber, pageRank, lemurResults[i].score, titleToScore[articleNumberStr], combinedScore);
        results.push_back(result);
    }

    normalizeResults();
    adjustPageRank();
}

/**
 * Prints the ranked list of results to stdout.
 * @param numResults - the max number of results to display to the screen
 */
void WikiRanker::displayResults(size_t numResults) 
{
    if(results.size() < 1)
    {
        cout << "[WikiRanker]: Sorry, no results found :(" << endl;
        return;
    }

    if(!quiet)
    {
        cout << "[WikiRanker]: displaying maximum of " << numResults << " results" << endl;
        cout << "\n\033[1mCombined Score  Lemur Score   Page Rank     Title Rank    Page Title \033[0m " << endl;
    }
    size_t numSuccesses;
    vector<Result>::iterator result;
    for(result = results.begin(), numSuccesses = 0; result != results.end() && numSuccesses < numResults; ++result)
    {
        if(!shouldFilter(result->pageTitle))
        {
            if(quiet)
            {
                cout << result->pageTitle << endl;
            }
            else
            {
                cout << left << std::setw(16) << result->combinedScore 
                     << left << std::setw(14) << result->lemurScore
                     << left << std::setw(14) << result->pageRank
                     << left << std::setw(14) << result->titleScore
                     << left << std::setw(26) << result->pageTitle
                     << endl;
            }
            ++numSuccesses;
        }
    }

    if(!quiet)
        cout << endl;
}

/**
 * Helper function for normalize; finds the max and min score
 *  for page ranks and lemur scores.
 */
void WikiRanker::findMaxMin(double & maxPR, double & minPR, double & maxLM, double & minLM, double & maxTS, double & minTS) const
{
    minPR = minLM = minTS = DBL_MAX; 
    maxPR = maxLM = maxTS = DBL_MIN;
    vector<Result>::const_iterator iter;
    for(iter = results.begin(); iter != results.end(); ++iter)
    {
        if(iter->pageRank < minPR)
            minPR = iter->pageRank;
        if(iter->pageRank > maxPR)
            maxPR = iter->pageRank;
        if(iter->lemurScore < minLM)
            minLM = iter->lemurScore;
        if(iter->lemurScore > maxLM)
            maxLM = iter->lemurScore;
        if(iter->titleScore < minTS)
            minTS = iter->titleScore;
        if(iter->titleScore > maxTS)
            maxTS = iter->titleScore;
    }
}

/**
 * Determines whether a page title should be filtered.
 * @param title - the page title in question
 * @return whether it should be ignored in the results list
 */
bool WikiRanker::shouldFilter(string title) const
{
    size_t colonIndex = title.find(":");
    if(colonIndex != string::npos)
    {
        string prefix = title.substr(0, colonIndex);
        return unwantedPrefixes.find(prefix) != unwantedPrefixes.end();
    }
    return false;
}

/**
 * Normalizes results in the range [0, 1].
 */
void WikiRanker::normalizeResults()
{
    double maxPR, minPR;
    double maxLM, minLM;
    double maxTS, minTS;
    findMaxMin(maxPR, minPR, maxLM, minLM, maxTS, minTS);
    vector<Result>::iterator iter;
    for(iter = results.begin(); iter != results.end(); ++iter)
    {
        iter->lemurScore = (iter->lemurScore - minLM) / (maxLM - minLM);
        iter->pageRank = (iter->pageRank - minPR) / (maxPR - minPR);
        iter->titleScore = (iter->titleScore - minTS) / (maxTS - minTS);
    }
}

/**
 * Takes the page rank into account for each result and resorts the results.
 */
void WikiRanker::adjustPageRank()
{
    vector<Result>::iterator result;
    if(useTitleScore == "yes" || useTitleScore == "yes ")
    {
        for(result = results.begin(); result != results.end(); ++result)
            result->combinedScore = result->lemurScore  * result->pageRank * result->titleScore;
          //result->combinedScore = (lambda * result->lemurScore) + ((1 - lambda) * result->pageRank);
    }
    else
    {
        for(result = results.begin(); result != results.end(); ++result)
            result->combinedScore = result->lemurScore  * result->pageRank;
    }
    std::sort(results.begin(), results.end(), Result::sortByCombined);
}

/**
 * Reads a config file and sets variables accordingly.
 */
void WikiRanker::readConfig()
{
    unordered_map<string, string> options = ConfigReader::read();

    lemurIndex = options["lemurIndex"];
    useTitleScore = options["useTitleScore"];
    lemurTitleIndex = options["lemurTitleIndex"];
    string lambdaStr = options["lambda"];
    std::istringstream(lambdaStr) >> lambda;
    string numResultsStr = options["numOriginalResults"];
    std::istringstream(numResultsStr) >> numOriginalResults;
    
    quiet = (options["quiet"] == "yes " || options["quiet"] == "yes");
    if(!quiet)
    {
        cout << "[WikiRanker]: using " << numOriginalResults << " for original query size" << endl;
        cout << "[WikiRanker]: using lambda value " << lambda << endl;
        cout << "[Lemur]:      initiated title query environment with index \"" << lemurTitleIndex << "\"" << endl;
        cout << "[WikiRanker]: using title score: " << options["useTitleScore"] << endl;
        cout << "[Lemur]:      initiated query environment with index \"" << lemurIndex << "\"" << endl;
    }
}
