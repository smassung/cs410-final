#!/bin/bash

# if wikisearch executable does not exist, make it
if [ ! -e wikisearch ]
then
    make
fi

# get the command line args into the query
webquery=""
query=""
for word in $@
do
    webquery=$webquery$word+
    query="$query $word"
done
wikiquery=$webquery
webquery=${webquery}site:wikipedia.org

# Get rid of the space in the beginning
query="${query:1}"

echo "Searching for \"$query\""

echo "Getting google results..."
wget -U Firefox/3.0.15? http://www.google.com/search?q=$webquery -O file.html -q
egrep -o 'a href="http://en.wikipedia.org[^"]+' file.html | sed 's/a.*wiki\///g' > google.txt

echo "Getting bing results..."
wget -U Firefox/3.0.15? http://www.bing.com/search?q=$webquery -O file.html -q
egrep -o 'a href="http://en.wikipedia.org[^"]+' file.html | sed 's/a.*wiki\///g' > bing.txt

echo "Getting wikipedia results..."
wget "http://en.wikipedia.org/w/index.php?&search=$wikiquery&fulltext=Search" -O file.html -q
egrep -o 'a href="/wiki/[^"]+' file.html | sed 's/a.*wiki\///g' > wikipedia.txt

echo "Getting wikisearch (no title score) results..."
./wikisearch $query | sed 's/ /_/g' > wikisearch-ts.txt
sed 's/useTitleScore yes/useTitleScore no/g' cpp/params/config.ini > temp.txt
mv temp.txt cpp/params/config.ini

echo "Getting wikisearch (with title score) results..."
./wikisearch $query | sed 's/ /_/g' > wikisearch+ts.txt
sed 's/useTitleScore no/useTitleScore yes/g' cpp/params/config.ini > temp.txt
mv temp.txt cpp/params/config.ini

echo "Getting ThunderSearch results..."
cd java
java -Xmx5G Search $query | sed 's/ /_/g' > ../thundersearch.txt
cd ..

# remove non-ascii characters
for file in *.txt
do
    tr -cd '\11\12\15\40-\176' < $file > $file.tmp
    rm -f $file
    mv $file.tmp $file
done

rm file.html
cat google.txt bing.txt wikisearch-ts.txt thundersearch.txt wikisearch+ts.txt wikipedia.txt | sort | uniq > all.txt
