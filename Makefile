APP= wikisearch
DB= createdb
APP_OBJS= cpp/wikiranker.o cpp/config_reader.o

prefix= /usr/local
exec_prefix= ${prefix}
libdir= ${exec_prefix}/lib
includedir= ${prefix}/include
INCPATH= -I$(includedir)
LIBPATH= -L$(libdir)
CXXFLAGS= -std=c++0x -DYYTEXT_POINTER=1 -DINDRI_STANDALONE=1 -DHAVE_NAMESPACES= -DISNAN_IN_NAMESPACE_STD= -DSTDC_HEADERS=1 -DHAVE_SYS_TYPES_H=1 -DHAVE_SYS_STAT_H=1 -DHAVE_STDLIB_H=1 -DHAVE_STRING_H=1 -DHAVE_MEMORY_H=1 -DHAVE_STRINGS_H=1 -DHAVE_INTTYPES_H=1 -DHAVE_STDINT_H=1 -DHAVE_UNISTD_H=1 -DHAVE_FSEEKO=1 -DHAVE_EXT_ATOMICITY_H=1 -DP_NEEDS_GNU_CXX_NAMESPACE=1 -DHAVE_MKSTEMP=1 -DHAVE_MKSTEMPS=1 -DNDEBUG=1 -O3 $(INCPATH)
CPPLDFLAGS=-lindri -lmongoclient -lz -lpthread -lm -lboost_thread -lboost_filesystem -lboost_program_options -lboost_system

all: $(APP) $(DB)

$(APP): $(APP_OBJS) cpp/wikisearch.cpp
	$(CXX) $(CXXFLAGS) cpp/wikisearch.cpp $(APP_OBJS) -o $(APP) $(LIBPATH) $(CPPLDFLAGS)

$(DB): cpp/pagerankdb.o cpp/pagerankdb.cpp
	$(CXX) $(CXXFLAGS) cpp/pagerankdb.o -o $(DB) $(LIBPATH) $(CPPLDFLAGS)

wikiranker.o: cpp/wikiranker.h cpp/wikiranker.cpp
	$(CXX) $(CXXFLAGS) -c cpp/wikiranker.cpp $(LIBPATH) $(CPPLDFLAGS)

config_reader.o: cpp/config_reader.h cpp/config_reader.cpp
	$(CXX) -std=c++0x -O3 -c cpp/config_reader.cpp

pagerankdb.o: cpp/pagerankdb.cpp
	$(CXX) $(CXXFLAGS) -c cpp/pagerankdb.cpp $(LIBPATH) $(CPPLDFLAGS)

clean:
	rm -f $(APP) $(DB) cpp/*.o java/*.class

tidy:
	rm -f wikisearch-ts.txt thundersearch.txt wikisearch+ts.txt google.txt bing.txt wikipedia.txt all.txt file.html

bed:
	@echo "What are you, stupid??"
